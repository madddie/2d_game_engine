#include "inc.h"

Input      g_input{};
//DX9Render g_render{};
DX11Render g_render{};
Pixel      g_pixel{};
Snake      g_snake{};

void main(int argc, char** argv)
{
    Window wnd({300,300,800,600}, WS_POPUPWINDOW, L"test1", L"test2");

    g_render.create(wnd);
    
    g_render.push_task([&]()
    {
        std::stringstream fps, frametime;

        fps << "fps: " << g_render.get_global_vars().m_fps;
        frametime << "frametime: " << g_render.get_global_vars().m_frametime;
   
        g_snake.think();
        g_snake.paint();

        if (g_snake.get_state() == game_state_t::ENABLED)
        {
            g_render.text(10, 10, 1.f, g_render.get_data().m_sprite_font.get(), Colors::Black, fps);
            g_render.text(10, 40, 1.f, g_render.get_data().m_sprite_font.get(), Colors::Black, frametime);
        }
    });
    

    wnd.push_task([&]()
    {
        g_render.paint();
    });

    return wnd.paint();
}