#include "inc.h"

bool DX11Render::create(Window& window)
{
    D3D_FEATURE_LEVEL feature_level;
    DXGI_SWAP_CHAIN_DESC swapchain_desc;

    m_width = window.get_window_data().m_width;
    m_height = window.get_window_data().m_height;

    // try to create our dx11 device
    if (D3D11CreateDevice(
        nullptr,
        D3D_DRIVER_TYPE_HARDWARE,
        0,
        0,
        nullptr,
        0,
        D3D11_SDK_VERSION,
        &m_data.m_device,
        &feature_level,
        &m_data.m_device_context)
        != S_OK)
        return false;

 
    // check that the feature level is correct
    if (feature_level != D3D_FEATURE_LEVEL_11_0)
        return false;

    // store the msaa quality level
    if (m_data.m_device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m_data.m_msaa_quality) != S_OK)
        return false;

    // check msaa quality
    assert(m_data.m_msaa_quality > 0);

    // setup swapchain desc
    swapchain_desc.BufferDesc.Width = m_width;
    swapchain_desc.BufferDesc.Height = m_height;
    swapchain_desc.BufferDesc.RefreshRate.Numerator = 144;
    swapchain_desc.BufferDesc.RefreshRate.Denominator = 1;
    swapchain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapchain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapchain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    swapchain_desc.SampleDesc.Count = 4;
    swapchain_desc.SampleDesc.Quality = m_data.m_msaa_quality - 1;

    swapchain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapchain_desc.BufferCount = 1;
    swapchain_desc.OutputWindow = window.get_window_data().m_handle;
    swapchain_desc.Windowed = true;
    swapchain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swapchain_desc.Flags = 0;

    IDXGIDevice* dev{};
    if (S_OK != m_data.m_device->QueryInterface<IDXGIDevice>(&dev))
        return false;

    IDXGIAdapter* adapter{};
    if (S_OK != dev->GetParent(__uuidof(decltype(adapter)), (void**)&adapter))
        return false;

    IDXGIFactory* factory{};
    if (S_OK != adapter->GetParent(__uuidof(decltype(factory)), (void**) &factory))
        return false;

    if (S_OK != factory->CreateSwapChain(m_data.m_device, &swapchain_desc, &m_data.m_swap_chain))
        return false;

    release_com(dev);
    release_com(adapter);
    release_com(factory);

    m_data.m_blend_state_desc.RenderTarget[0].BlendEnable = TRUE;
    m_data.m_blend_state_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    m_data.m_blend_state_desc.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_ALPHA;
    m_data.m_blend_state_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    m_data.m_blend_state_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
    m_data.m_blend_state_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    m_data.m_blend_state_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    m_data.m_blend_state_desc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

    if (S_OK != m_data.m_device->CreateBlendState(&m_data.m_blend_state_desc, &m_data.m_alpha_on_blend_state))
        return false;

    if (S_OK != m_data.m_device->CreateBlendState(&m_data.m_blend_state_desc, &m_data.m_alpha_off_blend_state))
        return false;

    m_data.m_common_states = std::make_unique<CommonStates>(m_data.m_device);
    m_data.m_sprite_batch = std::make_unique<SpriteBatch>(m_data.m_device_context);
    m_data.m_effect_factory = std::make_unique<EffectFactory>(m_data.m_device);
    m_data.m_sprite_font = std::make_unique<SpriteFont>(m_data.m_device, L"Arial12.spritefont");
    m_data.m_prim_batch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(m_data.m_device_context);

    m_data.m_batch_effect = std::make_unique<BasicEffect>(m_data.m_device);
    m_data.m_batch_effect->SetVertexColorEnabled(true);

    const void* shader_code;
    size_t code_len;

    m_data.m_batch_effect->GetVertexShaderBytecode(&shader_code, &code_len);

    if (S_OK != m_data.m_device->CreateInputLayout(VertexPositionColor::InputElements, VertexPositionColor::InputElementCount, shader_code, code_len, &m_data.m_input_layout))
        return false;

    reset();

    return true;
}

bool DX11Render::wnd_proc(uint32_t msg, WPARAM w, LPARAM l)
{
    switch (msg)
    {
    case WM_SIZE:
        if (m_data.m_device)
        {
            m_width = (int) LOWORD(l);
            m_height = (int) HIWORD(l);

            if (w == SIZE_MINIMIZED)
                m_minimized = true;
            else if (w == SIZE_MAXIMIZED)
            {
                m_minimized = false;
                reset();
            }
            else if (w == SIZE_RESTORED)
            {
                if (m_minimized)
                {
                    m_minimized = false;
                    reset();
                }
                else if (m_resizing)
                {

                }
                else
                {
                    reset();
                }
            }
        }
        return 0;

    case WM_ENTERSIZEMOVE:
        m_resizing = true;
        return 0;

    case WM_EXITSIZEMOVE:
        m_resizing = false;
        reset();
        return 0;

    }

    return 0;
}

void DX11Render::reset()
{
    XMMATRIX proj;
    ID3D11Texture2D* back_buffer;

    proj = XMMatrixOrthographicOffCenterRH(0.0f, m_width, m_height, 0.f, 0.f, 1.f);

    m_data.m_batch_effect->SetProjection(proj);
    m_data.m_batch_effect->SetWorld(XMMatrixIdentity());
    m_data.m_batch_effect->SetView(XMMatrixIdentity());

    assert(m_data.m_device_context);
    assert(m_data.m_device);
    assert(m_data.m_swap_chain);

    release_com(m_data.m_render_target_view);

    if (S_OK != m_data.m_swap_chain->ResizeBuffers(1, m_width, m_height, DXGI_FORMAT_R8G8B8A8_UNORM, 0))
        return;

    if (S_OK != m_data.m_swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&back_buffer))
        return;

    if (S_OK != m_data.m_device->CreateRenderTargetView(back_buffer, 0, &m_data.m_render_target_view))
        return;

    release_com(back_buffer);

    m_data.m_device_context->OMSetRenderTargets(1, &m_data.m_render_target_view, nullptr);

    m_data.m_viewport.TopLeftX = 0;
    m_data.m_viewport.TopLeftY = 0;
    m_data.m_viewport.Width = m_width;
    m_data.m_viewport.Height = m_height;
    m_data.m_viewport.MinDepth = 0.f;
    m_data.m_viewport.MaxDepth = 1.f;

    m_data.m_device_context->RSSetViewports(1, &m_data.m_viewport);
}

void DX11Render::paint()
{
    static Timer<std::chrono::milliseconds> second_timer;
    Timer<std::chrono::microseconds> frame_timer;

    assert(m_data.m_device_context);
    assert(m_data.m_swap_chain);

    std::array<float, 4> m_blend{}, m_clear
    {
        0.f, 0.f, 0.f, 0.f
    }; 

    // clear the scene
    m_data.m_device_context->ClearRenderTargetView(m_data.m_render_target_view, m_clear.data());
    m_data.m_device_context->OMSetBlendState(m_data.m_alpha_on_blend_state, m_blend.data() , (UINT)-1);

    // iterate and execute per-frame callbacks
    for (auto& callback : m_callbacks)
    {
        callback();
    }

    m_data.m_device_context->OMSetBlendState(m_data.m_alpha_off_blend_state, m_blend.data(), (UINT) -1);
    m_data.m_swap_chain->Present(0, 0);

    m_global_vars.m_framecount++;
    m_global_vars.m_frametime = (double) (frame_timer.diff() * .000001f);
    
    // fps according to frametime
    // m_global_vars.m_fps = 1.f / m_global_vars.m_frametime;

    // fps according to actual frames accumulated per second
    if (second_timer.diff() >= 1000)
    {
        static auto framecount = m_global_vars.m_framecount;

        m_global_vars.m_fps = (double) (m_global_vars.m_framecount - framecount);

        framecount = m_global_vars.m_framecount;

        second_timer.reset();
    }
}

void DX11Render::text(int x, int y, float scale, SpriteFont* font, const XMVECTORF32& color, const std::stringstream& str)
{
    text(x, y, scale, font, color, str.str());
}

void DX11Render::text(int x, int y, float scale, SpriteFont* font, const XMVECTORF32& color, const std::string& str)
{
    XMFLOAT2 pos{(float) x, (float) y};

    m_data.m_sprite_batch->Begin(SpriteSortMode_Deferred);
    
    font->DrawString(m_data.m_sprite_batch.get(), util::multibyte_to_wide(str).c_str(), pos, color, 0.f, {0.f, 0.f}, scale);
    
    m_data.m_sprite_batch->End();
}

void DX11Render::quad(int x, int y, int w, int h, const XMVECTORF32& color)
{
    float tmpx = (float) x;
    float tmpy = (float) y;
    float tmpw = (float) w;
    float tmph = (float) h;

    std::array<VertexPositionColor, 4> verts
    {
        {
            {{tmpx, tmpy + tmph}, color},
            {{tmpx, tmpy}, color},
            {{tmpx + tmpw, tmpy + tmph}, color},
            {{tmpx + tmpw, tmpy}, color}
        }
    };

    m_data.m_device_context->OMSetBlendState(m_data.m_common_states->Opaque(), nullptr, (UINT) -1);
    m_data.m_device_context->OMSetDepthStencilState(m_data.m_common_states->DepthNone(), 0);
    m_data.m_device_context->RSSetState(m_data.m_common_states->CullNone());
    m_data.m_batch_effect->Apply(m_data.m_device_context);
    m_data.m_device_context->IASetInputLayout(m_data.m_input_layout);
    
    m_data.m_prim_batch->Begin();

    m_data.m_prim_batch->DrawQuad(verts[0], verts[1], verts[3], verts[2]);

    m_data.m_prim_batch->End();
}

