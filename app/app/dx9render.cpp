#include "inc.h"

bool DX9Render::create(Window& wnd)
{
    D3DPRESENT_PARAMETERS parameters{};

    m_data.m_d3d = Direct3DCreate9(D3D_SDK_VERSION);

    if (!m_data.m_d3d)
        return false;

    parameters.Windowed = true;
    parameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
    parameters.hDeviceWindow = wnd.get_window_data().m_handle;
    parameters.BackBufferFormat = D3DFMT_A8R8G8B8;
    parameters.BackBufferWidth = wnd.get_window_data().m_width;
    parameters.BackBufferHeight = wnd.get_window_data().m_height;
    parameters.EnableAutoDepthStencil = true;
    parameters.AutoDepthStencilFormat = D3DFMT_D16;

    m_data.m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, wnd.get_window_data().m_handle, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &parameters, &m_data.m_device);

    if (!m_data.m_device)
        return false;

    // try to call D3DXCreateFont on every font map
    return create_fonts();
}

bool DX9Render::create_fonts()
{
    auto create_font = [ & ](const std::wstring& name, size_t px, ID3DXFont** font, size_t fw = FW_NORMAL)
    {
        return !D3DXCreateFont(m_data.m_device, px, 0, fw, 1, 0, 1, 0, CLEARTYPE_NATURAL_QUALITY, FF_DONTCARE, name.c_str(), font);
    };

    // idk i may be retarded
    for (size_t i = 8; i <= 72; i += 4)
    {
        if (!create_font(L"Arial", i, &m_arial[i]))
        {
            errbox("D3DXCreateFont Arial failed");
            return false;
        }
        if (!create_font(L"Consolas", i, &m_consolas[i]))
        {
            errbox("D3DXCreateFont Consolas failed");
            return false;
        }
    }

    return true;
}

void DX9Render::paint()
{
    m_data.m_device->Clear(0, 0, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.f, 0);
    m_data.m_device->BeginScene();

    // iterate and execute per-frame callbacks
    for (auto& callback : m_callbacks)
    {
        callback();
    }

    m_data.m_device->EndScene();
    m_data.m_device->Present(0, 0, 0, 0);
}

void DX9Render::text(int x, int y, ID3DXFont* font, ulong_t color, const std::stringstream& str)
{
    text(x, y, font, color, str.str());
}

void DX9Render::text(int x, int y, ID3DXFont* font, ulong_t color, const std::string& str)
{
    RECT r{x, y, x + 50, y + 50};

    font->DrawTextA(0, str.c_str(), str.size(), &r, DT_NOCLIP, color);
}

void DX9Render::quad(int x, int y, int w, int h, ulong_t color)
{
    ulong_t fvf;

    std::array<vertex_t, 4> verts
    {
        {
            {x, y + h, color},
            {x, y, color},
            {x + w, y + h, color},
            {x + w, y, color}
        }
    };

    m_data.m_device->SetRenderState(D3DRS_ZENABLE, 0);
    m_data.m_device->GetFVF(&fvf);
    m_data.m_device->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
    m_data.m_device->SetPixelShader(nullptr);
    m_data.m_device->SetTexture(0, nullptr);
    m_data.m_device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, verts.data(), sizeof(vertex_t));
    m_data.m_device->SetFVF(fvf);
}

