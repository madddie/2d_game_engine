#include "inc.h"

Snake::Snake()
{
    // lol. easiest prand 
    srand(time(0));

    m_state = DISABLED;

    m_food.m_position = {rand() % tile_width, rand() % tile_height};

    m_snake.m_position = {tile_width / 2, tile_height / 2};
    m_snake.m_size = 1;
}

bool Snake::input(uint32_t msg, WPARAM w, LPARAM l)
{
    if (g_input.get_key(VK_INSERT))
        m_state = (m_state == ENABLED) ? DISABLED : ENABLED;

    if (m_state != ENABLED)
        return false;

    switch (msg)
    {
    case WM_KEYDOWN:
        switch (w)
        {
        case VK_UP:
            m_snake.m_state = UP;
            break;
        case VK_DOWN:
            m_snake.m_state = DOWN;
            break;
        case VK_LEFT:
            m_snake.m_state = LEFT;
            break;
        case VK_RIGHT:
            m_snake.m_state = RIGHT;
            break;
        default:
            break;
        }
    }

    return true;
}

void Snake::game_over()
{
    m_state = DISABLED;

    // reset vars
    m_snake.m_historical_positions.clear();
    m_snake.m_size = 1;
    m_snake.m_position = {tile_width / 2, tile_height / 2};

    m_snake.m_state = FROZEN;
}

void Snake::think()
{
    static Timer<std::chrono::milliseconds> timer;
    POINT orig_pos = m_snake.m_position;;

    if (m_state != ENABLED)
        return;

    if (timer.diff() < 250)
        return;
    else
        timer.reset();

    switch (m_snake.m_state)
    {
    case RIGHT:
        m_snake.m_position.x++;
        break;
    case UP:
        m_snake.m_position.y--;
        break;
    case DOWN:
        m_snake.m_position.y++;
        break;
    case LEFT:
        m_snake.m_position.x--;
        break;
    case FROZEN:
        return;
    default:
        break;
    }

    m_snake.m_historical_positions.push_back(orig_pos);

    if (m_snake.m_position.x < 0 ||
        m_snake.m_position.y < 0 ||
        m_snake.m_position.x >= tile_width ||
        m_snake.m_position.y >= tile_height)
    {
        game_over();

        return;
    }

    // we collided with the food so eat it
    if (m_snake.m_position.x == m_food.m_position.x && m_snake.m_position.y == m_food.m_position.y)
    {
        m_snake.m_size++;
        m_food.m_position = {rand() % tile_width, rand() % tile_height};
    }
    else if(m_snake.m_size > 1)
    {
        for (int i = 1; i < m_snake.m_size; ++i)
        {
            auto history_size = m_snake.m_historical_positions.size();
            auto historical_pos = m_snake.m_historical_positions[history_size - i];

            // we bit ourselves nigga
            if (m_snake.m_position.x == historical_pos.x && m_snake.m_position.y == historical_pos.y)
            {
                game_over();

                return; 
            }
        }
    }
}

void Snake::paint()
{
    const XMVECTORF32 green1     = {.19f, .49f, .09f, 1.f};
    const XMVECTORF32 green2     = {.19f, .56f, .09f, 1.f};
    const XMVECTORF32 food       = {.56f, .09f, .19f, 1.f};
    const XMVECTORF32 snake_head = {.07f, .16f, .5f, 1.f};
    const XMVECTORF32 snake      = {.09f, .19f, .56f, 1.f};

    if (m_state != ENABLED)
    {
        g_render.text(10, 10, 2.f, g_render.get_arial(), Colors::Red, "paused/game over");
        g_render.text(10, 70, 2.f, g_render.get_arial(), Colors::Red, "insert to start");

        return;
    }

    // draw grid of tiles
    for (int w = 0; w < tile_width; ++w)
    {
        for (int h = 0; h < tile_height; ++h)
        {
            auto x = w * tile_px_size;
            auto y = h * tile_px_size;

            auto col = !((w+h) % 2) ? green1 : green2;

            g_render.quad(x, y, tile_px_size, tile_px_size, col);
        }
    }

    // draw the food
    auto food_x = 10 + (m_food.m_position.x * tile_px_size);
    auto food_y = 10 + (m_food.m_position.y * tile_px_size);

    g_render.quad(food_x, food_y, food_size_px, food_size_px, food);

    // draw the snake
    for (int i = 0; i < m_snake.m_size; ++i)
    {
        auto history_size = m_snake.m_historical_positions.size();
        auto historical_pos = i ? m_snake.m_historical_positions[history_size - i] : m_snake.m_position;

        auto section_x = 5 + (historical_pos.x * tile_px_size);
        auto section_y = 5 + (historical_pos.y * tile_px_size);

        g_render.quad(section_x, section_y, snake_size_px, snake_size_px, i ? snake : snake_head);
    }
}
