#pragma once

// THIS SHOULDNT BE THIS DUMB.
// todo: renderer interface so i can swap renderers on the fly

using font_map_t = std::unordered_map<int, ID3DXFont*>;

namespace dx_color
{
    constexpr auto black = D3DCOLOR_ARGB(255, 0, 0, 0);
    constexpr auto white = D3DCOLOR_ARGB(255, 255, 255, 255);
    constexpr auto red   = D3DCOLOR_ARGB(255, 255, 0, 0);
    constexpr auto green = D3DCOLOR_ARGB(255, 0, 255, 0);
    constexpr auto blue  = D3DCOLOR_ARGB(255, 0, 0, 255);
}

struct vertex_t
{
    vertex_t(float x_, float y_, ulong_t color_ = dx_color::white, float z_ = 0.f, float w_ = 0.f) :
        x(x_), y(y_), z(z_), w(w_), color(color_) {}
    vertex_t(int x_, int y_, ulong_t color_ = dx_color::white, float z_ = 0.f, float w_ = 0.f) :
        x((float)x_), y((float)y_), z(z_), w(w_), color(color_) {}

    float x, y, z, w;
    ulong_t color;
};

class DX9Render
{
private:

    struct directx_data_t
    {
        IDirect3D9*       m_d3d;
        IDirect3DDevice9* m_device;
    } m_data;

    font_map_t m_arial;
    font_map_t m_consolas;

    std::vector<callback_t> m_callbacks;

public:

    DX9Render() :
        m_arial{}, m_consolas{}, m_callbacks{}, m_data{} {}

    DX9Render(Window& wnd)
    {
        if (!create(wnd))
        {
            errbox("failed to create dx9 renderer");

            return;
        }
    }

    ~DX9Render()
    {
        // iterate and free the map of fonts
        for (auto& arial : m_arial)
            arial.second->Release();

        if (m_data.m_device)
            m_data.m_device->Release();

        if (m_data.m_d3d)
            m_data.m_d3d->Release();
    }

    // bigass init func
    bool create(Window& wnd);
    bool create_fonts();

    // paint
    void paint();

    // getters
    const directx_data_t get_data()
    {
        return m_data;
    }

    // simple font getter
    auto get_arial(size_t px)
    {
        return m_arial[px];
    }
    auto get_consolas(size_t px)
    {
        return m_consolas[px];
    }

    // add tasks to paint loop
    void push_task(callback_t callback)
    {
        m_callbacks.push_back(callback);
    }

    // DEBUG draw functions
    // performance heavy and flawed
    // todo: CODE BETTER.

    // text with stream/raw string
    void text(int x, int y, ID3DXFont* font, ulong_t color, const std::stringstream& str);
    void text(int x, int y, ID3DXFont* font, ulong_t color, const std::string& str);
    
    // filled box. (could use clear at the expense of losing transparency
    void quad(int x, int y, int w, int h, ulong_t color);

    // text with a cheap outline. 
    // todo: use pixel shaders
    void outlined_text(int x, int y, ulong_t color, const std::string& str);

};

//extern DX9Render g_render;