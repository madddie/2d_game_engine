#pragma once

#include <directxmath.h>
#include <m_maddie.h>

#include "CommonStates.h"
#include "DDSTextureLoader.h"
#include "DirectXHelpers.h"
#include "Effects.h"
#include "GamePad.h"
#include "GeometricPrimitive.h"
#include "GraphicsMemory.h"
#include "Keyboard.h"
#include "Model.h"
#include "Mouse.h"
#include "PostProcess.h"
#include "PrimitiveBatch.h"
#include "ScreenGrab.h"
#include "SimpleMath.h"
#include "SpriteBatch.h"
#include "SpriteFont.h"
#include "VertexTypes.h"
#include "WICTextureLoader.h"

using namespace DirectX;


//#include <d3d9.h>
//#include <d3dx9.h>

#include <bitset>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")

//#pragma comment( lib, "d3d9.lib" )
//#pragma comment( lib, "d3dx9.lib" )


// very gay func.. totally awful
static void errbox(const std::string& caption)
{
    std::stringstream err_msg;

    err_msg << "[x] " << caption << " - error code [" << GetLastError() << "]";

    MessageBoxA(0, err_msg.str().c_str(), "[error]", 0);
}

namespace file_system = std::experimental::filesystem;

#pragma warning(disable:4838)
#pragma warning(disable:4800)

using callback_t = std::function<void()>;

#define release_com(x)\
{                     \
    if (x)            \
    {                 \
       x->Release();  \
       x = nullptr;   \
    }                 \
}                     \

#include "math.h"
#include "window.h"
//#include "dx9render.h" 
#include "dx11render.h"
#include "input.h"
#include "pixel.h"
#include "snake.h"