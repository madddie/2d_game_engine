#include "inc.h"

static long __stdcall window_proc(HWND h, uint32_t msg, WPARAM w, LPARAM l)
{
    switch (msg)
    {
    case WM_SIZE:
    case WM_ENTERSIZEMOVE:
    case WM_EXITSIZEMOVE:
        return g_render.wnd_proc(msg, w, l);
    case WM_MOUSEMOVE:
    case WM_KEYDOWN:
    case WM_KEYUP:
    case WM_RBUTTONDOWN:
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
        return g_input.wnd_proc(msg, w, l); 

    case WM_DESTROY:
        PostQuitMessage(EXIT_SUCCESS);
        return EXIT_SUCCESS;
    }

    return DefWindowProc(h, msg, w, l);
}

bool Window::create()
{
    WNDCLASSEXW window_class{};

    window_class.cbSize = sizeof(WNDCLASSEXW);
    window_class.lpfnWndProc = window_proc;
    window_class.hInstance = 0;
    window_class.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
    window_class.style = CS_HREDRAW | CS_VREDRAW;
    window_class.lpszClassName = m_data.m_classname.c_str();
    window_class.lpszMenuName = m_data.m_name.c_str();

    RegisterClassExW(&window_class);

    m_data.m_handle = CreateWindowExW
    (
        0,
        m_data.m_classname.c_str(),
        m_data.m_name.c_str(),
        m_data.m_style,
        m_data.m_position.left,
        m_data.m_position.top,
        (m_data.m_position.right - m_data.m_position.left),
        (m_data.m_position.bottom - m_data.m_position.top),
        0,
        0,
        0,
        0
    );

    if (!m_data.m_handle)
    {
        errbox("CreateWindow failed");
        return false;
    }

    SetWindowLongA(m_data.m_handle, GWL_EXSTYLE, GetWindowLongA(m_data.m_handle, GWL_EXSTYLE));
    SetLayeredWindowAttributes(m_data.m_handle, RGB(0, 0, 0), 255, ULW_COLORKEY | LWA_ALPHA);
    ShowWindow(m_data.m_handle, SW_SHOWNORMAL);

    return true;
}

void Window::paint()
{
    MSG msg{};

    while (msg.message != WM_QUIT)
    {
       try 
       {
           if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
           {
               TranslateMessage(&msg);
               DispatchMessage(&msg);
           }
           else
           {
               for (auto& callback : m_callbacks)
               {
                   callback();
               }
           }
       }
       catch (std::exception except)
       {
           errbox(except.what());

           return;
       }
    }

    return;
}
