#pragma once

// test class

class Pixel
{
private:

    POINT m_pt;

    bool m_enabled;

public:

    Pixel() : m_pt{}, m_enabled{false} {}

    bool input(uint32_t msg, WPARAM w, LPARAM l);

    void paint();
};

extern Pixel g_pixel;