#pragma once

namespace math
{
    template< typename t > __forceinline void clamp(t &val, const t &min, const t &max)
    {
        val = (t) _mm_cvtss_f32(_mm_min_ss(_mm_max_ss(_mm_set_ss(val), _mm_set_ss(min)), _mm_set_ss(max)));
    }
};