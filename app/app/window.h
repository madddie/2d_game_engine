#pragma once

class Window
{
private:
    struct window_data_t
    {
        // need to convert from xywh to lrtb
        window_data_t(RECT p, ulong_t s, const std::wstring& c, const std::wstring& n)
        {
            m_width = p.right;
            m_height = p.bottom;

            m_position.left = p.left;
            m_position.top = p.top;
            m_position.right = p.left + p.right;
            m_position.bottom = p.top + p.bottom;

            m_classname = c;
            m_name = n;
            m_style = s;
        }

        int m_width, m_height;

        RECT m_position;
        HWND m_handle;

        ulong_t m_style;
       
        std::wstring m_classname;
        std::wstring m_name;
    } m_data;

    std::vector<callback_t> m_callbacks;

public:
    // ctors
    Window(RECT pos, ulong_t style, const std::wstring& classname, const std::wstring& name) : m_data(pos, style, classname, name)
    {
        if (!create())
        {
            errbox("couldn't create window");
        }
    }
    
    // dtor
    ~Window()
    {
        // lol fuck it
        DestroyWindow(m_data.m_handle);
    }

    // big create function
    bool create();

    // paint
    void paint();

    // get/sets
    const auto& get_window_data()
    {
        return m_data;
    }

    // add tasks to the callbacks, uses lambdas or whatever
    void push_task(callback_t callback)
    {
        m_callbacks.push_back(callback);
    }
};