#pragma once

constexpr auto tile_width = 16;
constexpr auto tile_height = 12;
constexpr auto tile_px_size = 50;
constexpr auto food_size_px = 30;
constexpr auto snake_size_px = 40;

enum move_state_t
{
    FROZEN,
    UP,
    DOWN,
    LEFT,
    RIGHT
};

enum game_state_t
{
    ENABLED,
    DISABLED,
    GAME_OVER
};

class Snake
{
private:
    struct snake_t
    {
        std::vector<POINT> m_sections;
        std::vector<POINT> m_historical_positions;

        POINT m_position;

        uint32_t m_size;

        move_state_t m_state{FROZEN};
    } m_snake;

    struct food_t
    {
        POINT m_position;
    } m_food;

    game_state_t m_state;
public:
    Snake();

    bool input(uint32_t msg, WPARAM w, LPARAM l);

    void game_over();
    void think();
    void paint();

    __forceinline auto get_state()
    {
        return m_state;
    }
};

extern Snake g_snake;