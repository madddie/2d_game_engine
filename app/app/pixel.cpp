#include "inc.h"

bool Pixel::input(uint32_t msg, WPARAM w, LPARAM l)
{
    if (g_input.get_key(VK_INSERT))
        m_enabled = !m_enabled;

    if (!m_enabled)
        return false;

    switch (msg)
    {
    case WM_KEYDOWN:
        switch (w)
        {
        case VK_UP:
            m_pt.y -= 5;
            break;
        case VK_DOWN:
            m_pt.y += 5;
            break;
        case VK_LEFT:
            m_pt.x -= 5;
            break;
        case VK_RIGHT:
            m_pt.x += 5;
            break;
        default:
            break;
        }
    }

    math::clamp<LONG>(m_pt.x, 0, 800 - 20);
    math::clamp<LONG>(m_pt.y, 0, 600 - 20);

    return true;
}

void Pixel::paint()
{
    if (!m_enabled)
        return;

    //g_render.quad(m_pt.x, m_pt.y, 20, 20, dx_color::green);
}
